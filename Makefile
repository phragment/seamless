
.PHONY: run clean edit

all: run

run:
	./src/seamless.py -v -c config/default.ini

clean:
	-rm -r src/seamless/__pycache__

edit:
	gedit -s src/seamless.py src/seamless/base.py src/seamless/operator.py src/seamless/events.py src/seamless/model.py src/seamless/view.py 2> /dev/null &


