
# SeamLess

SeamLess is a window managing application launcher.

It provides integration with Qtile to allow starting applications
along with assigning and ordering of windows within Qtiles group layouts.

[Qtile configuration](https://gitlab.gnome.org/phragment/qtile-config)


Seamless uses:

- [PyGObject](https://pygobject.readthedocs.io/en/latest/)
- [Qtile](http://www.qtile.org/)


