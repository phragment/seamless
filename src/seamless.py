#!/usr/bin/env python3

# This file is part of SeamLess
# Copyright 2023 Thomas Krug
#
# SeamLess is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SeamLess is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

from seamless import base, operator, events


def main(log, settings):
    global OP
    OP = operator.Operator(log, settings)

    OP.enqueue(events.LoadApplications())
    OP.enqueue(events.LoadPresets())

    OP.enqueue(events.FocusSearchbar())

    OP.run()


def stop():
    OP.enqueue(events.Quit())


if __name__ == "__main__":
    scheme = {
        "preset-dir": {"type": "path", "default": "presets"},
    }

    app = base.Application("seamless", scheme, main, stop)
    app.run()



