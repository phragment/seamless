# This file is part of SeamLess
# Copyright 2023 Thomas Krug
#
# SeamLess is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SeamLess is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import subprocess
import time


class Event:

    def process(self, operator):
        raise NotImplementedError("no process function")


class Quit(Event):

    def process(self, operator):
        operator.log.debug("shutting down")
        operator.view.stop()
        operator.stop()


class LoadApplications(Event):

    def process(self, operator):
        operator.log.debug("loading applications")
        operator.model.load_applications()


class LoadPresets(Event):

    def process(self, operator):
        operator.log.debug("loading presets")
        operator.model.load_presets()


class FocusSearchbar(Event):

    def process(self, operator):
        operator.model.view.focus_searchbar()


class Run(Event):

    def __init__(self, cmd):
        self.cmd = cmd

    def process(self, operator):
        self._start(operator)

    def _start(self, operator):
        operator.log.debug("starting: %s", self.cmd)

        # TODO
        # check if group is "full"
        # -> create new sub group
        # move functionality to own qtile layout

        try:
          proc = subprocess.Popen(
              self.cmd,
              #shell=True,  # running in shell probably return wrong pid
              start_new_session=True,
              # if we pass nothing, isatty correctly detects non-interactive
              #stdin=subprocess.DEVNULL,
              #stdout=subprocess.DEVNULL,
              #stderr=subprocess.DEVNULL
          )
        except FileNotFoundError:
            operator.log.error("command not found: %s", self.cmd)
            return 0

        ppid = proc.pid
        operator.log.debug("started, pid %s", ppid)
        return ppid


class RunPreset(Run):

    def __init__(self, ident):
        super().__init__("")
        self.ident = ident

    def process(self, operator):
        operator.log.debug("starting preset: %s", self.ident)

        preset = operator.model.presets[self.ident]
        print(preset)

        # TODO check ids, use filename as id?!
        presetids = preset.get("presets", [])
        for presetid in presetids:
            operator.enqueue(RunPreset(presetid))

        groups = operator.model.qtile.list_groups()
        operator.log.debug("existing groups: %s", groups)

        group = preset.get("group", None)
        if group:
            if group in groups:
                operator.log.debug("group %s already exists", group)
                return

            operator.log.debug("create new group %s", group)
            operator.model.qtile.new_group(group)

        layout = preset.get("layout", None)
        if layout:
            operator.log.debug("set layout %s", layout)
            operator.model.qtile.set_layout(layout)

        for program in preset.get("programs", []):
            cmd = program.get("cmd", "")
            if not cmd:
                continue

            params = program.get("params", [])
            if params:
                cmd = [cmd]
                cmd.extend(params)

            self.cmd = cmd
            pid = self._start(operator)

            if not pid:
                return

            self.program = program
            self.operator = operator
            self.operator.model.qtile.notify_ready(self.on_ready)

            # FIXME dont wait for pid window
            self.operator.log.debug("waiting for %s window", pid)
            self.wait = True
            timeout = 30
            while self.wait:
                time.sleep(0.1)
                timeout -= 1
                if not timeout:
                    self.wait = False
                    self.operator.log.debug("timed out")

    def on_ready(self, pid):
        self.operator.log.debug("window mapped by %s", pid)

        # check if we or target pid is parent?


        # FIXME register this a rule and run async
        #   move window to correct group?!

        position = self.program.get("position", "")
        if position == "left":
            self.operator.model.qtile.move_left(pid)
        if position == "right":
            self.operator.model.qtile.move_right(pid)

        if position == "float":
            w = self.program.get("width", 0)
            h = self.program.get("height", 0)

            x = self.program.get("x", -1)
            y = self.program.get("y", -1)

            self.operator.model.qtile.move(pid, x, y, w, h)

        self.wait = False



class RunAndSetup(Run):

    def __init__(self, cmd):
        self.cmd = cmd

    def process(self, operator):

        # TODO register cb in model.qtile?

        pid = self._start(operator)

        # wait for new window to show up








