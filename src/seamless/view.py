# This file is part of SeamLess
# Copyright 2023 Thomas Krug
#
# SeamLess is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SeamLess is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later



import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib, Gdk, GObject

from seamless import events


# decorator for enqueuing functions into gtk main thread
def gtk(func):
    def wrapper(*args, **kwargs):
        #print("view wrapper, called by", threading.current_thread().name)
        GLib.idle_add(func, *args, **kwargs)
    return wrapper


class View:

    def __init__(self, log, settings, operator):
        self.log = log.getChild(type(self).__name__)
        self.settings = settings
        self.operator = operator

    def start(self):
        self.log.debug("starting")
        self.app = Application(self)
        self.app.run()

    @gtk
    def stop(self):
        self.log.debug("stopping")
        self.app.win.win.destroy()

    @gtk
    def add_completion_entries(self, entries):
        self.log.debug("adding completion entries")
        self.app.win.searchbar.add_entries(entries)

    @gtk
    def add_preset(self, ident, text):
        self.log.debug("adding preset: %s", text)
        self.app.win.presets.add_preset(ident, text)

    @gtk
    def focus_searchbar(self):
        self.log.debug("focus searchbar")
        self.app.win.searchbar.entry.grab_focus()

    """
    @gtk
    def save_window_state(self):
      size = self.app.win.win.get_default_size()
      self.log.debug(size)
      state = {"window": {"width": size.width, "height": size.height}}
      with open(self.settings["state"], "w") as f:
          json.dump(state, f, sort_keys=True, indent=4)
    """


class Application:

    def __init__(self, view):
        self.view = view
        self.app = Gtk.Application.new(None, 0)
        self.app.connect("activate", self.activate)

    def run(self):
        self.view.log.debug("application run")
        self.app.run()

    def activate(self, app):
        self.win = MainWindow(app, self.view)


class MainWindow:

    def __init__(self, app, view):
        self.view = view
        self.log = view.log

        self.win = Gtk.ApplicationWindow.new(app)

        self.win.set_title("SeamLess")
        #self.win.set_icon_name("")

        self.win.connect("close-request", self.on_window_close_request)

        self.win.set_default_size(300, 200)

        self.vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.win.set_child(self.vbox)

        self.presets = Presets(self.view)
        self.vbox.append(self.presets.get_widget())

        self.searchbar = SearchBar(self.view)
        self.vbox.append(self.searchbar.get_widget())

        """
        TODO
        hide window header
        set as dialog?
        """

        #self.win.set_titlebar(Gtk.Box())
        self.win.set_decorated(False)


        # TODO
        # notify qtile to make this window floating?


        ec_key = Gtk.EventControllerKey()
        ec_key.connect("key-released", self.on_key_released)
        self.win.add_controller(ec_key)

        self.win.show()

        self.log.info("startup finished")

    def on_window_close_request(self, window):
        self.view.operator.enqueue(events.Quit())
        # return event handled, otherwise window shuts down immediately
        return True

    def on_key_released(self, ec, keyval, keycode, state):
        if keyval == Gdk.KEY_Escape:
            self.view.operator.enqueue(events.Quit())


class Presets:

    def __init__(self, view):
        self.view = view
        self.log = view.log

        self.widget = Gtk.ScrolledWindow()

        self.flowbox = Gtk.FlowBox()
        self.flowbox.props.vexpand = True
        self.widget.set_child(self.flowbox)

        self.flowbox.props.homogeneous = True

    def get_widget(self):
        return self.widget

    def add_preset(self, ident, text):
        self.log.debug("creating preset button")

        button = Gtk.Button()
        button.set_label(text)
        button.connect("clicked", self.on_button_clicked, ident)
        self.flowbox.append(button)

    def on_button_clicked(self, button, ident):
        self.log.debug("starting %s", ident)

        self.view.operator.enqueue(events.RunPreset(ident))
        # TODO put quit after run as param to run event?
        #self.view.operator.enqueue(events.Quit())


class SearchBar:

    def __init__(self, view):
        self.view = view
        self.log = view.log

        self.entry = Gtk.Entry()

        self.completion = Gtk.EntryCompletion()
        self.model = Gtk.ListStore(str)
        self.completion.set_model(self.model)
        self.completion.set_text_column(0)

        self.completion.set_inline_completion(True)
        self.completion.set_inline_selection(True)
        self.completion.props.popup_single_match = False
        self.completion.props.minimum_key_length = 1

        self.entry.set_completion(self.completion)
        self.entry.connect("activate", self.on_entry_activated)

    def get_widget(self):
        return self.entry

    def add_entries(self, entries):
        for entry in entries:
            it = self.model.append()
            self.model.set_value(it, 0, entry)

    def on_entry_activated(self, entry):
        cmd = entry.get_text()

        self.view.operator.enqueue(events.Run(cmd))
        # TODO put quit after run as param to run event?
        #self.view.operator.enqueue(events.Quit())





















