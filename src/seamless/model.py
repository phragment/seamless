# This file is part of SeamLess
# Copyright 2023 Thomas Krug
#
# SeamLess is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SeamLess is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import os
import time
import json

import gi
from gi.repository import Gio

import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop

# python-psutil
import psutil

from seamless import events


class Model:

    def __init__(self, log, settings, view):

        self.log = log.getChild(type(self).__name__)
        self.settings = settings
        self.view = view

        self.qtile = Qtile()

        self.presets = {}

    def load_applications(self):
        #a = time.time()
        infos = Gio.AppInfo.get_all()
        #b = time.time()
        #print(b-a, len(infos))

        #print(infos[0])

        #for info in infos:
        #    info.list_actions()

        #name = info.get_name()
        #dn = info.get_display_name()
        #cmd = info.get_commandline()
        #exe = info.get_executable()
        #info.get_is_hidden()
        #info.get_nodisplay()
        #info.should_show()


        entries = []
        for info in infos:
            if not info.should_show():
                continue
            dn = info.get_display_name()
            cmd = info.get_executable()
            #entries.append([dn, cmd])

            if os.path.isabs(cmd):
                cmd = os.path.basename(cmd)

            entries.append(cmd)

        entries = sorted(list(set(entries)))

        self.view.add_completion_entries(entries)

    def load_presets(self):
        dp = self.settings["preset-dir"]
        self.log.debug("loading presets from %s", dp)

        if not os.path.exists(dp):
            self.log.debug("no preset dir")
            return

        for fn in os.listdir(dp):
            self.log.debug("parsing %s", fn)
            fp = os.path.join(dp, fn)
            f = open(fp, "r")
            try:
              preset = json.load(f)
            except json.decoder.JSONDecodeError as e:
              self.log.error("failed parsing: %s", e)
            f.close()


            k, v = list(preset.items())[0]
            #print(k, v)
            self.presets[k] = v

            # TODO make keys unique
            #self.view.add_preset(k, v["name"])
            self.view.add_preset(k, k)



class Qtile:

    def __init__(self):
        self.bus = dbus.SessionBus(mainloop=DBusGMainLoop())
        try:
            self.service = self.bus.get_object("org.qtile.inofficial", "/org/qtile")
        except dbus.exceptions.DBusException:
            self.service = None

        # TODO check version

        self.callbacks = []
        self.register_signal()

    def list_groups(self):
        if not self.service:
            return

        method = self.service.get_dbus_method("list_groups", "org.qtile")
        result = method()  # return dbus.array containing dbus.string

        groupnames = []
        for s in result:
            groupnames.append(str(s))

        return groupnames

    def new_group(self, name):
        if not self.service:
            return

        method = self.service.get_dbus_method("new", "org.qtile.group")
        result = method(name)

    def set_layout(self, name):
        if not self.service:
            return

        method = self.service.get_dbus_method("set", "org.qtile.layout")
        result = method(name)

    def notify_ready(self, cb):
        self.callbacks.append(cb)

    def register_signal(self):
        if not self.service:
            return

        self.service.connect_to_signal("new_window", self.on_new_window, dbus_interface="org.qtile")

    def on_new_window(self, pid):
        # only manipulate window if it belongs to us
        launcherpid = os.getpid()
        children = [child.pid for child in psutil.Process(launcherpid).children(True)]
        if not pid in children:
            #self.operator.log.debug("pid %s not started by us", pid)
            return

        for cb in self.callbacks:
            cb(pid)
            del cb

    def move(self, pid, x, y, w, h):
        if not self.service:
            return

        method = self.service.get_dbus_method("move_float", "org.qtile.window")
        result = method(pid, x, y, w, h)

    def move_left(self, pid):
        if not self.service:
            return

        method = self.service.get_dbus_method("move_left", "org.qtile.window")
        result = method(pid)

    def move_right(self, pid):
        if not self.service:
            return

        method = self.service.get_dbus_method("move_right", "org.qtile.window")
        result = method(pid)





