# This file is part of SeamLess
# Copyright 2023 Thomas Krug
#
# SeamLess is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SeamLess is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import collections
import threading

from seamless import model, view, events


class Operator:
    instance = None

    def __init__(self, log, settings):
        self.log = log.getChild(type(self).__name__)
        self.settings = settings

        self.queue = Queue(self.log)
        self.running = True

        self.view = view.View(log, settings, self)
        self.model = model.Model(log, settings, self.view)

        Operator.instance = self

    def run(self):
        op_thread = threading.Thread(target=self.process, name="OpThread")
        op_thread.start()

        # Gtk has to be run from thread which created the objects
        self.view.start()

        op_thread.join()

        self.log.info("shutdown finished")

    def process(self):
        self.log.debug("starting in " + threading.current_thread().name)
        while self.running:
            event = self.queue.get()

            if not isinstance(event, events.Event):
                self.log.warning("unknown event: " + str(event))
                continue

            event.process(self)

    def stop(self):
        self.running = False

    def enqueue(self, cmd):
        self.queue.put(cmd)


class Queue:

    def __init__(self, log):
        self.log = log

        # thread-safe and memory efficient queue providing
        # fast appends and pops from either side
        self.queue = collections.deque()

        # used to let the consumer sleep, NOT for locking
        self.sem = threading.Semaphore(value=0)

    def put(self, cmd):
        self.log.debug("put " + str(cmd))
        self.queue.append(cmd)
        self.sem.release()

    def get(self):
        self.sem.acquire()
        return self.queue.popleft()

    def peek(self):
        try:
            return self.queue[0]
        except IndexError:
            return None

